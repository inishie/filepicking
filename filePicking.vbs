'ファイルピッキングツール
'ドラッグ＆ドロップしたファイル名を元に指定フォルダからファイルを収集します。
' ex) cscript //nologo filePicking.vbs filelist.txt >> filelog.log

Option Explicit

Dim objApl          'Object
Dim objFileSys      'Object
Dim objRE           'Object
Dim objListFile     '指示ファイル
Dim strCopyFrom     'コピー元
Dim strCopyTo       'コピー先フォルダ
Dim objArgs         '引数
Dim sInputFilename  '引数ファイル名
Dim objFolder
Dim objFolderItems
Dim strHitFile      '検出ファイルパス(戻値)
Dim rootPath
Dim needFolder      'フォルダ作成の要否
Dim strCopyToSub    'フォルダ作成時のサブ
Dim isFileCopy      'ファイルコピーフラグ
Dim isRegularExpression  '正規表現使用フラグ

Set objArgs = WScript.Arguments
Set objFileSys = CreateObject("Scripting.FileSystemObject")
Set objApl = WScript.CreateObject("Shell.Application")
Set objRE = CreateObject("VBScript.RegExp")

rootPath = objFileSys.getParentFolderName(WScript.ScriptFullName) & "\"

' 設定値読込
ExecuteGlobal objFileSys.OpenTextFile(rootPath & "filePickingConfig.ini").ReadAll()

'ドラッグ＆ドロップ
If objArgs.Count <> 1 Then
	'引数は必須
	WScript.Echo "一覧ファイルを指定してください"
	WScript.Quit 1
End If
If (False = objFileSys.FileExists(objArgs(0))) Then
	'引数は必須
	WScript.Echo "一覧ファイルを指定してください"
	WScript.Quit 1
End If
If (strCopyFrom = "" Or strCopyTo = "") Then
	'環境設定は必須
	WScript.Echo "環境設定してください"
	WScript.Quit 1
End If
If (False = objFileSys.FolderExists(strCopyFrom)) Then
	'コピー元は必須
	WScript.Echo "移動元のフォルダ指定が無効です"
	WScript.Quit 1
End If
If (isFileCopy And (False = objFileSys.FolderExists(strCopyTo))) Then
	'コピー先を用意する
	objFileSys.CreateFolder(strCopyTo)
End If

'探りを入れたいフォルダのオブジェクトを作成します
Set objFolder = objApl.NameSpace(strCopyFrom)

'フォルダオブジェクトから、入っているファイルやフォルダの情報を取得します
Set objFolderItems = objFolder.Items()

'https://www.whitire.com/vbs/tips0072.html
Set objListFile = objFileSys.OpenTextFile(objArgs(0))
If Err.Number = 0 Then
	Do While objListFile.AtEndOfStream <> True

		sInputFilename = objListFile.ReadLine
		If isRegularExpression = False Then
			sInputFilename = objFileSys.getFileName( sInputFilename )
		End If

		strHitFile = ""
		If sInputFilename <> "" Then

			'フォルダの検索処理の呼び出し
			Call prcFolderSize (objFolderItems, sInputFilename)

			If strHitFile = "" Then
				'該当データなし
				Call outputLog(sInputFilename, "", "")
			End If
		End If

	Loop
	objListFile.Close
Else
	WScript.Echo "指示ファイルのオープンエラー: " & Err.Description
End If

Set objRE = Nothing
Set objFileSys = Nothing
Set objFolderItems = Nothing
Set objFolder = Nothing
Set objApl = Nothing

'==============================================================
'= フォルダ内に含まれるファイルやフォルダを検索する
'= 最初に検出(ファイル名一致)したパスを変数に設定する
'= コピー込み
'==============================================================
Sub prcFolderSize(tmpFolderItems, fileName)

	Dim objFolderItemsB
	Dim objItem
	Dim i, AgreementFlg

	'�C何個入っていたかは、Countで参照
	For i = 0 To tmpFolderItems.Count - 1

		'�Dファイルやフォルダの情報を１コ取り出します
		Set objItem = tmpFolderItems.Item(i)

		'�E取り出した物がファイルかフォルダかを判定
		If objItem.IsFolder Then
			'�Fフォルダであれば、フォルダアイテムオブジェクトを作り、それを引数としてprcFolderSizeを再帰呼び出します
			Set objFolderItemsB = objItem.GetFolder
			Call prcFolderSize (objFolderItemsB.Items(), fileName)
			Else
			'�Gファイルの場合
			AgreementFlg = isAgreement(objFileSys.getFileName(objItem), fileName, objItem.Path)
			If AgreementFlg Then
				strHitFile = objItem.Path
				If isNeedFolder Then
					'フォルダ維持
					strCopyToSub = ""
					If isFileCopy Then
						'コピーする
						strCopyToSub = getCopytoFolder(strHitFile, strCopyFrom, strCopyTo)
					End If
					Call fileCopy(fileName, strHitFile, strCopyToSub)
					' 続ける
				Else
					'直下配置
					Call fileCopy(fileName, strHitFile, strCopyTo)
					Exit For '抜ける
				End If
			End If
		End If

	Next

	Set objItem = Nothing
	Set objFolderItemsB = Nothing

End Sub

'コピー先のフォルダ特定
Function getCopytoFolder(fromFile, fromFolder, toFolder)

	getCopytoFolder = ""

	getCopytoFolder = objFileSys.GetParentFolderName(fromFile)
	If getCopytoFolder <> fromFolder Then
		' フォルダ直下でない
		getCopytoFolder = Right(getCopytoFolder, LEN(getCopytoFolder) - LEN(fromFolder))
		If (Len(getCopytoFolder) > 0) Then
			'有効
			getCopytoFolder = toFolder & Right(getCopytoFolder, LEN(getCopytoFolder) - 1) & "\"
		End If
	Else
		' フォルダ直下
		getCopytoFolder = toFolder
	End If

End Function

'ファイルコピー
Sub fileCopy(fileName, fromFile, toFolder)
	Dim toFile, fromFolder, strDummy, isValidity

	isValidity = False
	fromFolder = objFileSys.GetParentFolderName(fromFile)
	On Error Resume Next
		strDummy = objFileSys.GetFolder(fromFolder)
		If Err.Number = 0 Then
			'有効なフォルダ
			isValidity = True
		End If
	On Error Goto 0

	If isValidity And isFileCopy Then
		'コピー実行可能

		If (False = objFileSys.FolderExists(toFolder)) Then
			CreateDir(toFolder)
		End If

		objFileSys.CopyFile fromFile, toFolder
		toFile = toFolder & objFileSys.getFileName( fromFile )
	End If

	'ログ出力
	Call outputLog(fileName, fromFile, toFile)

End Sub

'通常のログ出力
Sub outputLog(targetFile, fromFile, toFile)

	WScript.echo targetFile & vbTab & fromFile & vbTab & toFile

End Sub

'指定されたパスにディレクトリが無かった場合
'ディレクトリを作成します。
'https://pnpk.net/cms/?p=308
Sub CreateDir(Path)
	If Len(Path) > 0 Then
		' パラメータが正しい

		Dim strParent
		strParent = objFileSys.GetParentFolderName(Path)

		If Not objFileSys.FolderExists(strParent) Then
			CreateDir(strParent)
		End If

		If Not objFileSys.FolderExists(Path) Then
			objFileSys.CreateFolder(Path)
		End If
	End If

End Sub

' 指定条件に合致しているか判定
Function isAgreement(fileName1, fileName2, fullpath)

	isAgreement = False
	If objFileSys.FileExists(fullpath) Then
		'ファイルとして妥当(zip内検索の除外)
		If isRegularExpression Then
			isAgreement = VbLike(fileName1, fileName2)
		Else
			isAgreement = (fileName1 = fileName2)
		End If
	End If

End Function

'正規表現による比較
'http://chaichan.lolipop.jp/vbtips/VBMemo2007010118.htm
Function VbLike(sTest, sPattern)
	objRE.Pattern = "^" & sPattern & "$"
	If objRE.test(sTest) = True Then
		VbLike = True
	Else
		VbLike = False
	End If
End Function
