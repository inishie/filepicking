@echo off
cd /d  %~dp0

if "%~1"=="" (
	echo Please specify the file.
	timeout 4
	exit /B 1
)

set YYYYMMDD=%DATE:/=%
set TIME_TEMP=%TIME: =0%
set HHNNSS=%TIME_TEMP:~0,2%%TIME_TEMP:~3,2%%TIME_TEMP:~6,2%
set LOG_FILE=list-%YYYYMMDD%-%HHNNSS%.log

cscript //nologo filePicking.vbs "%~1" >> %LOG_FILE%

type %LOG_FILE%

echo Finished normally.
timeout 4
exit /B 0
