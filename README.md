
**filePicking**

	ドラッグ＆ドロップしたファイル名を元に指定フォルダからファイルを収集します。

---

[TOC]

---

## Usage

	ファイル名を列挙したテキストファイルをvbsの引数に指定して起動します。
	ドラッグ＆ドロップ操作用にバッチファイルを追加しています。
	ex) cscript //nologo filePicking.vbs filelist.txt >> filelog.log

	ファイルの抽出方法は iniファイル で指定できます。
	filePickingConfig.ini
		strCopyFrom : コピー元フォルダ ex: "C:\temp1"
		isFileCopy : ファイルコピーフラグ(False:検出のみ, True:コピーする)
		strCopyTo : コピー先フォルダ  ex: rootPath & "work\"
		isNeedFolder : フォルダ作成の要否(True:必要、サブフォルダを構成する, False:不要、直下に並べる)
		isRegularExpression : 指示ファイルの正規表現使用フラグ(True:正規表現[RegExp]を使用, False:拡張子を含んで完全一致)


## Note

	・vbs, bat, ini は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・コピー元に圧縮ファイル(*.zip)を含んでいる場合、その中身はファイル名の検索対象外です。

---

## References

	shikaku
	ドラッグ＆ドロップされたファイル名を取得 - shikaku's blog
	http://blog.systemjp.net/entry/20080414/p1

	Microsoft
	ExecuteGlobal ステートメント | Microsoft Docs
	https://msdn.microsoft.com/ja-jp/library/cc392448.aspx

	pnpk
	VBスクリプトでディレクトリ階層を一回で作成する方法 | https://pnpk.net
	https://pnpk.net/cms/?p=308

	tonton
	[ファイル終端まで１行ずつ読み込む] - VBScript Tips (Tips0072)
	https://www.whitire.com/vbs/tips0072.html

	ChaichanPapa
	VBScript（VBスクリプト）でLikeするには？
	http://chaichan.lolipop.jp/vbtips/VBMemo2007010118.htm

	Q11Q
	VBScriptにないLikeを作るVBLike - Qiita
	https://qiita.com/Q11Q/items/15c1166715f913e1efbd


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
